module TestPackage
export foo, bar

foo(x) = 2x
bar(y) = y^2
foobar(x) = foo(bar(x))
checkity_check(x) = println(x)


end

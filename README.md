# TestPackage

[![Build Status](https://gitlab.com/theogf/TestPackage.jl/badges/main/pipeline.svg)](https://gitlab.com/theogf/TestPackage.jl/pipelines)
[![Coverage per line](https://gitlab.com/theogf/TestPackage.jl/badges/main/coverage.svg)](https://gitlab.com/theogf/TestPackage.jl/commits/main)
[![Coverage Codecov](https://codecov.io/gl/theogf/testpackage.jl/branch/main/graph/badge.svg?token=1IX4ZT4K0S)](https://codecov.io/gl/theogf/testpackage.jl)
[![Coverage coveralls](https://coveralls.io/repos/gitlab/theogf/testpackage.jl/badge.svg)](https://coveralls.io/gitlab/theogf/testpackage.jl)

using TestPackage
using Test

@testset "TestPackage.jl" begin
    @test foo(2) == 4
    @test foo(1.0) == 2.0
end
